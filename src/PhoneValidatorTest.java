import org.junit.jupiter.api.Test;

import java.util.Date;

public class PhoneValidatorTest {

    PhoneValidator validator = new PhoneValidator();

    @Test
    public void validateTest_tooShort() {
        assert validator.validate("+4852") == Wynik.ZA_MALO_ZNAKOW;
    }

    @Test
    public void validateTest_tooLong() {
        assert validator.validate("+485212345678910") == Wynik.ZA_DUZO_ZNAKOW;
    }

    @Test
    public void validateTest_wrongSize() {
        assert validator.validate("+4812345678") == Wynik.NIE_POPRAWNA_ILOSC_ZNAKOW;
        assert validator.validate("0123456789") == Wynik.NIE_POPRAWNA_ILOSC_ZNAKOW;
    }

    @Test
    public void validateTest_tooLong12() {
        assert validator.validate("012345678901") == Wynik.ZA_DUZO_ZNAKOW;
    }
    @Test
    void validateTest_wrongChars9() {
        assert validator.validate("+12345678") == Wynik.NIE_OBSLUGIWANE_ZNAKI;
        assert validator.validate("1234/5678") == Wynik.NIE_OBSLUGIWANE_ZNAKI;
    }

    @Test
    void validateTest_correct9() {
        assert validator.validate("456789123") == Wynik.PRAWIDLOWY_BEZ_KODU_KRAJU;
    }

    @Test
    void validateTest_wrongChars12() {
        assert validator.validate("+48123456+23") == Wynik.NIE_OBSLUGIWANE_ZNAKI;
        assert validator.validate("+4845678901/") == Wynik.NIE_OBSLUGIWANE_ZNAKI;
    }

    @Test
    void validateTest_correct12() {
        assert validator.validate("+48456789123") == Wynik.PRAWIDLOWY_Z_KODEM_KRAJU;
    }
}
