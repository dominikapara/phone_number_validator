public class PhoneValidator {

    Wynik validate(String input) {

        if (input.length() < 9) {
            return Wynik.ZA_MALO_ZNAKOW;
        }

        if (input.length() > 12) {
            return Wynik.ZA_DUZO_ZNAKOW;
        }

        if (input.length() == 10 || input.length() == 11) {
            return Wynik.NIE_POPRAWNA_ILOSC_ZNAKOW;
        }

        if (input.length() == 12 && !input.startsWith("+48")) {
            return Wynik.ZA_DUZO_ZNAKOW;
        }

        if (input.length() == 9) {
            for (char znak : input.toCharArray()) {
                try {
                    Integer.parseInt(String.valueOf(znak));
                } catch (Exception e) {
                    return Wynik.NIE_OBSLUGIWANE_ZNAKI;
                }
            }

            return Wynik.PRAWIDLOWY_BEZ_KODU_KRAJU;
        }
        //jeśli funkcja dotarła do tego miejsca, to input.length() == 12 i zaczyna się od '+48' (linia 15)
        for (char znak : input.substring(3).toCharArray()) {
            try {
                Integer.parseInt(String.valueOf(znak));
            } catch (Exception e) {
                return Wynik.NIE_OBSLUGIWANE_ZNAKI;
            }
        }
        return Wynik.PRAWIDLOWY_Z_KODEM_KRAJU;
    }
}
